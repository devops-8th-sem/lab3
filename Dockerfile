FROM python:3.10-alpine3.17

COPY ./app/requirements.txt /requirements.txt

RUN apk add --update --no-cache libpq-dev python3-dev gcc musl-dev \
    && pip install --upgrade pip \
    && pip install psycopg2-binary \
    && pip install -r /requirements.txt
