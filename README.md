# Lab3

### Open source web app used from here
https://github.com/Rjtsahu/sample-django-app

### Generate the server.key and server.crt
```
openssl req -new -text -passout pass:abcd -subj /CN=localhost -out server.req
openssl rsa -in privkey.pem -passin pass:abcd -out server.key
openssl req -x509 -in server.req -text -key server.key -out server.crt
```
Put the resulting cert files in ./ssl/ directory

### Set postgres (alpine) user as owner of the server.key and permissions to 600
```
sudo chown 0:70 server.key
sudo chmod 640 server.key
```

### Build the image and bring up the stack
```
docker-compose up -d --build
```

### Tear down the stack
```
docker-compose down
```